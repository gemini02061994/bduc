﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BDUC.ITFaculty.Web.UI.Startup))]
namespace BDUC.ITFaculty.Web.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
