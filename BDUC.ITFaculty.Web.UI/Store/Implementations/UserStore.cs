﻿using BDUC.ITFaculty.Web.UI.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BDUC.ITFaculty.Web.UI.Store.Implementations
{
    public class UserStore : Interfaces.IBDUCUserStore
    {
        SharedRepositories.Interfaces.IUserRepository _Repository;
        
        #region Constructors
        public UserStore()
        {
            _Repository = new SharedRepositories.Implementations.UserRepository();
        }

        public UserStore(SharedRepositories.Interfaces.IUserRepository repository)
        {
            _Repository = repository;
        }

        #endregion

        #region IUserStore Implementation
        public Task CreateAsync(User user)
        {
            return _Repository.CreateAsync(user);
        }

        public Task DeleteAsync(User user)
        {
            return _Repository.DeleteAsync(user);
        }

        public void Dispose()
        {
            _Repository.Dispose();
        }

        public Task<User> FindByIdAsync(string userId)
        {
            return _Repository.FindByIdAsync(userId);
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return _Repository.FindByNameAsync(userName);
        }

        public Task UpdateAsync(User user)
        {
            return _Repository.UpdateAsync(user);
        }
        #endregion

        #region IUserRoleStore Implementation
        public Task<IList<string>> GetRolesAsync(User user)
        {
            return _Repository.GetRolesAsync(user);
        }

        public Task<bool> IsInRoleAsync(User user, string roleName)
        {
            return _Repository.IsInRoleAsync(user, roleName);
        }

        public Task RemoveFromRoleAsync(User user, string roleName)
        {
            return _Repository.RemoveFromRoleAsync(user, roleName);
        }

        public Task AddToRoleAsync(User user, string roleName)
        {
            return _Repository.AddToRoleAsync(user, roleName);
        }
        #endregion

        # region IUserPasswordStore Implementation
        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            return _Repository.SetPasswordHashAsync(user, passwordHash);
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return _Repository.GetPasswordHashAsync(user);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return _Repository.HasPasswordAsync(user);
        }
        #endregion

        #region IUserPhoneNumberStore Implementation

        public Task SetPhoneNumberAsync(User user, string phoneNumber)
        {
            return _Repository.SetPhoneNumberAsync(user, phoneNumber);
        }

        public Task<string> GetPhoneNumberAsync(User user)
        {
            return _Repository.GetPhoneNumberAsync(user);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(User user)
        {
            return _Repository.GetPhoneNumberConfirmedAsync(user);
        }

        public Task SetPhoneNumberConfirmedAsync(User user, bool confirmed)
        {
            return _Repository.SetPhoneNumberConfirmedAsync(user, confirmed);
        }

        #endregion

        #region IQueryableUserStore Implementation
        public IQueryable<User> Users
        {
            get
            {
                return _Repository.Users;
            }
        }
        #endregion

        #region IUserLoginStore Implementation
        public Task AddLoginAsync(User user, UserLoginInfo login)
        {
            return _Repository.AddLoginAsync(user, login);
        }

        public Task RemoveLoginAsync(User user, UserLoginInfo login)
        {
            return _Repository.RemoveLoginAsync(user, login);
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(User user)
        {
            return _Repository.GetLoginsAsync(user);
        }

        public Task<User> FindAsync(UserLoginInfo login)
        {
            return _Repository.FindAsync(login);
        }
        #endregion
        #region IUserTwoFactorStore Implementation
        public Task SetTwoFactorEnabledAsync(User user, bool enabled)
        {
            return _Repository.SetTwoFactorEnabledAsync(user, enabled);
        }

        public Task<bool> GetTwoFactorEnabledAsync(User user)
        {
            return _Repository.GetTwoFactorEnabledAsync(user);
        }
        #endregion

        #region IUserLockoutStore Implementation
        public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        {
            return _Repository.GetLockoutEndDateAsync(user);
        }

        public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        {
            return _Repository.SetLockoutEndDateAsync(user, lockoutEnd);
        }

        public Task<int> IncrementAccessFailedCountAsync(User user)
        {
            return _Repository.IncrementAccessFailedCountAsync(user);
        }

        public Task ResetAccessFailedCountAsync(User user)
        {
            return _Repository.ResetAccessFailedCountAsync(user);
        }

        public Task<int> GetAccessFailedCountAsync(User user)
        {
            return _Repository.GetAccessFailedCountAsync(user);
        }

        public Task<bool> GetLockoutEnabledAsync(User user)
        {
            return _Repository.GetLockoutEnabledAsync(user);
        }

        public Task SetLockoutEnabledAsync(User user, bool enabled)
        {
            return _Repository.SetLockoutEnabledAsync(user, enabled);
        }
        #endregion

        #region IUserEmailStore implementation
        public Task SetEmailAsync(User user, string email)
        {
            return _Repository.SetEmailAsync(user, email);
        }

        public Task<string> GetEmailAsync(User user)
        {
            return _Repository.GetEmailAsync(user);
        }

        public Task<bool> GetEmailConfirmedAsync(User user)
        {
            return _Repository.GetEmailConfirmedAsync(user);
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed)
        {
            return _Repository.SetEmailConfirmedAsync(user, confirmed);
        }

        public Task<User> FindByEmailAsync(string email)
        {
            return _Repository.FindByEmailAsync(email);
        }
        #endregion
    }
}