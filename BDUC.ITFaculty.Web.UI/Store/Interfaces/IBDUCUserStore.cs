﻿using BDUC.ITFaculty.Web.UI.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.ITFaculty.Web.UI.Store.Interfaces
{
    interface IBDUCUserStore : IUserStore<User>,
                            IUserRoleStore<User, string>,
                            IUserPasswordStore<User, string>,
                            IUserPhoneNumberStore<User, string>,
                            IQueryableUserStore<User, string>,
                            IUserLoginStore<User, string>,
                            IUserTwoFactorStore<User, string>,
                            IUserLockoutStore<User, string>,
                            IUserEmailStore<User, string>
    {

    }
}
