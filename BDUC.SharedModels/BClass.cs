﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedModels
{
    public class BClass : IModel<int>
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public virtual IEnumerable<BStudent> Students { get; set; }
        public virtual BLecturer Lecturer { get; set; }
    }
}