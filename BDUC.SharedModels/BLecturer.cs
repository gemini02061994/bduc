﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedModels
{
    public class BLecturer : IModel<int>
    {
        public int Id { get; set; }
        public EnumAcademicRanks AcademicRank { get; set; }
        public EnumDegrees Degree { get; set; }
        public virtual BUser User { get; set; }
        public virtual IEnumerable<BClass> TeachingClasses { get; set; }
    }
}
