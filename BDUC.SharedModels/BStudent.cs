﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedModels
{
    public class BStudent : IModel<int>
    {
        public int Id { get; set; }
        public string StudentCode { get; set; }
        public virtual IEnumerable<BClass> Class { get; set; }
        public virtual BUser User { get; set; }
    }
}