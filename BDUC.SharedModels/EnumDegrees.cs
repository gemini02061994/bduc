﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedModels
{
    public enum EnumDegrees
    {
        Bachelor = 1,
        Masters = 2,
        Doctor  = 3,
    }
}
