﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedModels
{
    public enum EnumStudentState
    {
        Studying = 1,
        Graduated = 2,
        Reserved = 3,

        /// <summary>
        /// Give up before graduated
        /// </summary>
        Withdrew = 4
    }
}
