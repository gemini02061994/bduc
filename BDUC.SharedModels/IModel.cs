﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedModels
{
    public interface IModel<IdType>
    {
        IdType Id { get; set; }
    }
}
