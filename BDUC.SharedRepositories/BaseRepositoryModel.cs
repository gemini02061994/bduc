﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedRepositories
{
    public class BaseRepositoryModel<T> : IRepository<T>
    {
        public T Insert(T entity)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(IEnumerable<T> entities)
        {
            throw new NotImplementedException();
        }

        public bool Remove(T entity)
        {
            throw new NotImplementedException();
        }

        public bool RemoveAll(IEnumerable<T> entities)
        {
            throw new NotImplementedException();
        }

        public bool Update(T entitiy)
        {
            throw new NotImplementedException();
        }
    }
}
