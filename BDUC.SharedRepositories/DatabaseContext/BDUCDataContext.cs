﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Common;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace BDUC.SharedRepositories.DatabaseContext
{
    public class BDUCDataContext : DbContext
    {
        public static String ConnectionString = "Data Source=.;Initial Catalog=BDUC;Integrated Security=True";

        public DbSet<SharedModels.BClass> Classes { get; set; }
        public DbSet<SharedModels.BStudent> Students { get; set; }
        public DbSet<SharedModels.BUser> Users { get; set; }
        public DbSet<SharedModels.BLecturer> Lecturers { get; set; }

        #region Constructors
        public BDUCDataContext(string connectionstring) : base(connectionstring)
        {
            
        }
        #endregion
    }
}
