﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedRepositories
{
    public interface IRepository<T>
    {
        T Insert(T entity);
        void InsertAll(IEnumerable<T> entities);

        bool Remove(T entity);
        bool RemoveAll(IEnumerable<T> entities);

        bool Update(T entitiy);
    }
}
