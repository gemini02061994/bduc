﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedRepositories.Interfaces
{
    public interface IUserRepository : IRepository<SharedModels.BSharedModels.BUser>
    {
        #region IUserStore Implementation
        
        Task CreateAsync(SharedModels.BUser user);

         Task DeleteAsync(SharedModels.BUser user);

        void Dispose();

        Task<SharedModels.BUser> FindByIdAsync(string userId);

        Task<SharedModels.BUser> FindByNameAsync(string userName);

        Task UpdateAsync(SharedModels.BUser user);
        #endregion

        #region IUserRoleStore Implementation
        Task<IList<string>> GetRolesAsync(SharedModels.BUser user);

        Task<bool> IsInRoleAsync(SharedModels.BUser user, string roleName);

        Task RemoveFromRoleAsync(SharedModels.BUser user, string roleName);



         Task AddToRoleAsync(SharedModels.BUser user, string roleName);

        #endregion

        #region IUserPasswordStore Implementation
        Task SetPasswordHashAsync(SharedModels.BUser user, string passwordHash);
        Task<string> GetPasswordHashAsync(SharedModels.BUser user);
        Task<bool> HasPasswordAsync(SharedModels.BUser user);
        #endregion

        #region IUserPhoneNumberStore Implementation

        Task SetPhoneNumberAsync(SharedModels.BUser user, string phoneNumber);

        Task<string> GetPhoneNumberAsync(SharedModels.BUser user);
        Task<bool> GetPhoneNumberConfirmedAsync(SharedModels.BUser user);
        Task SetPhoneNumberConfirmedAsync(SharedModels.BUser user, bool confirmed);
        #endregion

        #region IQueryableUserStore Implementation
         IQueryable<SharedModels.BUser> Users
        {
            get
            {
                return _Repository.Users;
            }
        }
        #endregion

        #region IUserLoginStore Implementation
        Task AddLoginAsync(SharedModels.BUser user, UserLoginInfo login);

        Task RemoveLoginAsync(SharedModels.BUser user, UserLoginInfo login);

        Task<IList<UserLoginInfo>> GetLoginsAsync(SharedModels.BUser user);

        Task<SharedModels.BUser> FindAsync(UserLoginInfo login);
        #endregion
        #region IUserTwoFactorStore Implementation
        Task SetTwoFactorEnabledAsync(SharedModels.BUser user, bool enabled);

        Task<bool> GetTwoFactorEnabledAsync(SharedModels.BUser user);
        #endregion

        #region IUserLockoutStore Implementation
        Task<DateTimeOffset> GetLockoutEndDateAsync(SharedModels.BUser user);
        Task SetLockoutEndDateAsync(SharedModels.BUser user, DateTimeOffset lockoutEnd);
        Task<int> IncrementAccessFailedCountAsync(SharedModels.BUser user);
        Task ResetAccessFailedCountAsync(SharedModels.BUser user);

        Task<int> GetAccessFailedCountAsync(SharedModels.BUser user);
        Task<bool> GetLockoutEnabledAsync(SharedModels.BUser user);
        Task SetLockoutEnabledAsync(SharedModels.BUser user, bool enabled);
        #endregion

        #region IUserEmailStore implementation
        Task SetEmailAsync(SharedModels.BUser user, string email);
        Task<string> GetEmailAsync(SharedModels.BUser user);

        Task<bool> GetEmailConfirmedAsync(SharedModels.BUser user);
        Task SetEmailConfirmedAsync(SharedModels.BUser user, bool confirmed);
        Task<SharedModels.BUser> FindByEmailAsync(string email);
        #endregion
    }
}
