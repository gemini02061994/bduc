﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDUC.SharedRepositories
{
    public class MigrationsContextFactory : IDbContextFactory<DatabaseContext.BDUCDataContext>
    {
        public DatabaseContext.BDUCDataContext Create()
        {
            return new DatabaseContext.BDUCDataContext(DatabaseContext.BDUCDataContext.ConnectionString);
        }
    }
}
