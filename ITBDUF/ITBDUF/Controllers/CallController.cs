﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;

namespace ITBDUF.Controllers
{
    public class CallController : Controller
    {
        // GET: Call
        public ActionResult Index()
        {
            return View();
        }

        public string Call()
        {
            // Set our Account SID and AuthToken
            string accountSid = "ACba3da15db6563b620e897940d589c0ba";
            string authToken = "433095e1dda8edd29cf35b54422920b1";

            // A phone number you have previously validated with Twilio
            string phonenumber = "+16692312483";

            // Instantiate a new Twilio Rest Client
            var client = new TwilioRestClient(accountSid, authToken);

            // Initiate a new outbound call
            var call = client.InitiateOutboundCall(
                phonenumber, // The number of the phone initiating the call
                "+84993142171", // The number of the phone receiving call
                "http://demo.twilio.com/welcome/voice/" // The URL Twilio will request when the call is answered
            );

            if (call.RestException == null)
            {
                Response.Write(string.Format("Started call: {0}", call.Sid));
            }
            else
            {
                Response.Write(string.Format("Error: {0}", call.RestException.Message));
            }
            return "1";
        }

    }
}