﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ITBDUF.Controllers.CustomFilter
{
    /// <summary>
    /// This type of action will run before and after code in acion method
    /// </summary>
    public class TestActionFilter: ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            System.Diagnostics.Debug.WriteLine("TestActionFilter: OnActionExcuted : /"+ filterContext.Controller +"/"+ filterContext.ActionDescriptor);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            System.Diagnostics.Debug.WriteLine("TestActionFilter: OnActionExecuting : /" + filterContext.Controller + "/" + filterContext.ActionDescriptor);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            System.Diagnostics.Debug.WriteLine("TestActionFilter: OnActionExecuting : /" + filterContext.Controller);
        }
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            System.Diagnostics.Debug.WriteLine("TestActionFilter: OnActionExecuting : /" + filterContext.Controller);
        }
    }
}