﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ITBDUF.Controllers
{
    public class DemoAjaxHelperController : Controller
    {
        // GET: DemoAjaxHelper
        
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Duration = 0, VaryByCustom ="")]
        public ActionResult UpdateWithAjax()
        {
            return PartialView();
        }
    }
}