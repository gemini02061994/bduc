﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITBDUF.Models
{
    public class Class : IModel<int>
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public virtual IEnumerable<Student> Students { get; set; }
        public virtual Lecturer Lecturer { get; set; }
    }
}