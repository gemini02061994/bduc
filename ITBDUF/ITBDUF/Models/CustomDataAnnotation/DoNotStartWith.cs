﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ITBDUF.Models.CustomDataAnnotation
{
    public class DoNotStartWith : ValidationAttribute
    {
        char _InvalidCharacter;

        public DoNotStartWith(char invalidCharacter)
        {
            _InvalidCharacter = invalidCharacter;
        }

        public override bool IsValid(object value)
        {
            string StrValue =  value as string;
            if (StrValue==null && StrValue.Length == 0)
            {
                return true;
            }
            if (StrValue[0] == _InvalidCharacter)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}