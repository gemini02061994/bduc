﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;

namespace ITBDUF.Models.CustomIdentity.Respository
{
    public class CustomRoleRespository : IRoleRespository
    {
        ITBDUFDBContext _Context;
        DbSet<Role> _Roles;

        #region Constructor
        public CustomRoleRespository()
        {
            _Context = new ITBDUFDBContext();
            _Roles = _Context.Roles;
        }

        public CustomRoleRespository(ITBDUFDBContext context)
        {
            _Context = context;
            _Roles = _Context.Roles;
        }
        #endregion

        public IQueryable<Role> Roles
        {
            get
            {
                return _Context.Roles;
            }
        }

        public Task CreateAsync(Role role)
        {
            return Task.Run(() =>
            {
                _Roles.Add(role);
                _Context.SaveChanges();
            });
        }

        public Task DeleteAsync(Role role)
        {
            return Task.Run(() =>
            {
                _Roles.Remove(role);
                _Context.SaveChangesAsync();
            });
        }

        public void Dispose()
        {
            _Context.Dispose();
        }

        public Task<Role> FindByIdAsync(string roleId)
        {
            return _Roles.FirstOrDefaultAsync(r => r.Id == roleId);
        }

        public Task<Role> FindByNameAsync(string roleName)
        {
            return _Roles.FirstOrDefaultAsync(r => r.Name == roleName);
        }

        public Task UpdateAsync(Role role)
        {
            _Context.Roles.Attach(role);
            _Context.Entry(role).State = EntityState.Modified;
            return _Context.SaveChangesAsync();
        }
    }
}