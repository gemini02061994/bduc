﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITBDUF.Models.CustomIdentity.Respository
{
    public interface IRoleRespository : IRespository<Role>, IRoleStore<Role, string>, IQueryableRoleStore<Role, string>
    {
       
    }
}
