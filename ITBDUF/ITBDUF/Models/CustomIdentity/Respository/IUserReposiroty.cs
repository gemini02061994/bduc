﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITBDUF.Models.CustomIdentity.Respository
{
    public interface IUserReposiroty : IRespository<User>,
                            IUserStore<User>,
                            IUserRoleStore<User, string>,
                            IUserPasswordStore<User, string>,
                            IUserPhoneNumberStore<User, string>,
                            IQueryableUserStore<User, string>,
                            IUserLoginStore<User, string>,
                            IUserTwoFactorStore<User, string>,
                            IUserLockoutStore<User, string>,
                            IUserEmailStore<User, string>
    {

    }
}
