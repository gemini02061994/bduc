﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace ITBDUF.Models.CustomIdentity.Respository
{
    public class UserRespository : IUserReposiroty
    {

        ITBDUFDBContext _Context;
        DbSet<User> _Users;

        #region Constructor
        public UserRespository()
        {
            _Context = new ITBDUFDBContext();
            _Users = _Context.Users;
        }

        public UserRespository(ITBDUFDBContext context)
        {
            _Context = context;
            _Users = _Context.Users;
        }
        #endregion

        public IQueryable<User> Users
        {
            get
            {
                return _Context.Users;
            }
        }

        public Task AddLoginAsync(User user, UserLoginInfo login)
        {
            user.Logins.Add(new CustomUserLogin(login));
            return _Context.SaveChangesAsync();
        }

        public Task AddToRoleAsync(User user, string roleName)
        {
            user.Roles.Add(new Role { Name = roleName });
            return _Context.SaveChangesAsync();
        }

        public Task CreateAsync(User user)
        {
            user.Id = Guid.NewGuid().ToString();
            _Users.Add(user);
            return _Context.SaveChangesAsync();
        }

        public Task DeleteAsync(User user)
        {
            _Users.Remove(user);
            return _Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _Context.Dispose();
        }

        public Task<User> FindAsync(UserLoginInfo login)
        {
            return _Users.FirstOrDefaultAsync(u => u.Logins.Any(l => l.ProviderKey == login.ProviderKey && l.LoginProvider == l.LoginProvider));
        }

        public Task<User> FindByEmailAsync(string email)
        {
            return _Users.FirstOrDefaultAsync(u => u.Email == email);
        }

        public Task<User> FindByIdAsync(string userId)
        {
            return _Users.FirstOrDefaultAsync(u => u.Id == userId);
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return _Users.FirstOrDefaultAsync(u => u.UserName == userName);
        }
        
        public Task<int> GetAccessFailedCountAsync(User user)
        {
            return Task.Run(() =>
            {
                return user.AccessFailCount;
            });
        }

        public Task<string> GetEmailAsync(User user)
        {
            return Task.Run(() =>
            {
                return user.Email;
            });
        }

        public Task<bool> GetEmailConfirmedAsync(User user)
        {
            return Task.Run(new Func<bool>(() =>
            {
                return user.EmailConfirmed;
            }));
        }

        public Task<bool> GetLockoutEnabledAsync(User user)
        {
            return Task.Run(() =>
            {
                return user.LockOutEnabled;
            });
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        {
            return Task.Run(new Func<DateTimeOffset>(() =>
            {
                return user.LockOutEndDate;
            }));
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(User user)
        {
            return Task.Run(new Func<IList<UserLoginInfo>>(() =>
            {
                List<CustomUserLogin> logins = user.Logins.ToList();
                List<UserLoginInfo> rtList = new List<UserLoginInfo>(logins.Count);
                foreach (var item in logins)
                {
                    rtList.Add(new UserLoginInfo(item.LoginProvider, item.ProviderKey));
                }
                return rtList;
            }));
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return Task.Run(new Func<string>(() =>
            {
                return user.PasswordHash;
            }));
        }

        public Task<string> GetPhoneNumberAsync(User user)
        {
            return Task.Run(new Func<string>(() =>
            {
                return user.PhoneNumber;
            }));
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(User user)
        {
            return Task.Run(new Func<bool>(() =>
            {
                return user.PhoneNumberConfirmed;
            }));
        }

        public Task<IList<string>> GetRolesAsync(User user)
        {
            return Task.Run(new Func<IList<string>>(() =>
            {
                return user.Roles.Select(r => r.Name).ToList();
            }));
        }

        public Task<bool> GetTwoFactorEnabledAsync(User user)
        {
            return Task.Run(new Func<bool>(() =>
            {
                return user.TwoFactorEnabled;
            }));
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return Task.Run(new Func<bool>(() =>
            {
                return !string.IsNullOrWhiteSpace(user.PasswordHash);
            }));
        }

        public Task<int> IncrementAccessFailedCountAsync(User user)
        {
            return Task.Run(new Func<int>(() =>
            {
                user.AccessFailCount += 1;
                return user.AccessFailCount;
            }));
        }

        public Task<bool> IsInRoleAsync(User user, string roleName)
        {
            return Task.Run(new Func<bool>(() =>
            {
                return user.Roles.Any(r => r.Name == roleName);
            }));
        }

        public Task RemoveFromRoleAsync(User user, string roleName)
        {
            Role role = user.Roles.SingleOrDefault(r => r.Name == roleName);
            if (role != null)
            {
                user.Roles.Remove(role);
                return _Context.SaveChangesAsync();
            }
            return null;
        }

        public Task RemoveLoginAsync(User user, UserLoginInfo login)
        {
            CustomUserLogin log = user.Logins.SingleOrDefault(l => l.LoginProvider == login.LoginProvider && l.ProviderKey == login.ProviderKey);
            if (log != null)
            {
                user.Logins.Remove(log);
                return _Context.SaveChangesAsync();
            }
            return null;
        }

        public Task ResetAccessFailedCountAsync(User user)
        {
            user.AccessFailCount = 0;
            return _Context.SaveChangesAsync();
        }

        public Task SetEmailAsync(User user, string email)
        {
            user.Email = email;
            return _Context.SaveChangesAsync();
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed)
        {
            user.EmailConfirmed = confirmed;
            return _Context.SaveChangesAsync();
        }

        public Task SetLockoutEnabledAsync(User user, bool enabled)
        {
            user.LockOutEnabled = enabled;
            return _Context.SaveChangesAsync();
        }

        public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        {
            user.LockOutEndDate = lockoutEnd;
            return _Context.SaveChangesAsync();
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return _Context.SaveChangesAsync();
        }

        public Task SetPhoneNumberAsync(User user, string phoneNumber)
        {
            user.PhoneNumber = phoneNumber;
            return _Context.SaveChangesAsync();
        }

        public Task SetPhoneNumberConfirmedAsync(User user, bool confirmed)
        {
            user.PhoneNumberConfirmed = confirmed;
            return _Context.SaveChangesAsync();
        }

        public Task SetTwoFactorEnabledAsync(User user, bool enabled)
        {
            user.TwoFactorEnabled = enabled;
            return _Context.SaveChangesAsync();
        }

        public Task UpdateAsync(User user)
        {
            _Context.Users.Attach(user);
            _Context.Entry(user).State = EntityState.Modified;
            return _Context.SaveChangesAsync();
        }
    }
}