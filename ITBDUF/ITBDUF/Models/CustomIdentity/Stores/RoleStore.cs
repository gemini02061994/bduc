﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace ITBDUF.Models.CustomIdentity.Stores
{
    public class RoleStore : IRoleStore<Role, string>, IQueryableRoleStore<Role, string>
    {
        Respository.IRoleRespository _Respository;
        public RoleStore()
        {
            _Respository = new Respository.CustomRoleRespository();
        }

        public RoleStore(Respository.IRoleRespository res)
        {
            _Respository = res;
        }

        public IQueryable<Role> Roles
        {
            get
            {
                return _Respository.Roles;
            }
        }

        public Task CreateAsync(Role role)
        {
            return _Respository.CreateAsync(role);
        }

        public Task DeleteAsync(Role role)
        {
            return _Respository.DeleteAsync(role);
        }

        public void Dispose()
        {
            _Respository.Dispose();
        }

        public Task<Role> FindByIdAsync(string roleId)
        {
            return _Respository.FindByIdAsync(roleId);
        }

        public Task<Role> FindByNameAsync(string roleName)
        {
            return _Respository.FindByNameAsync(roleName);
        }

        public Task UpdateAsync(Role role)
        {
            return _Respository.UpdateAsync(role);
        }
    }
}