﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ITBDUF.Models.CustomIdentity.Stores
{
    public class UserStore : IUserStore<User>,
                            IUserRoleStore<User, string>,
                            IUserPasswordStore<User, string>,
                            IUserPhoneNumberStore<User, string>,
                            IQueryableUserStore<User, string>,
                            IUserLoginStore<User, string>,
                            IUserTwoFactorStore<User, string>,
                            IUserLockoutStore<User, string>,
                            IUserEmailStore<User, string>
    {

        Respository.IUserReposiroty _Respository;


        #region Constructors
        public UserStore()
        {
            _Respository = new Respository.UserRespository();
        }

        public UserStore(Respository.IUserReposiroty res)
        {
            _Respository = res;
        }

        #endregion

        #region IUserStore Implementation
        public Task CreateAsync(User user)
        {
            return _Respository.CreateAsync(user);
        }

        public Task DeleteAsync(User user)
        {
            return _Respository.DeleteAsync(user);
        }

        public void Dispose()
        {
            _Respository.Dispose();
        }

        public Task<User> FindByIdAsync(string userId)
        {
            return _Respository.FindByIdAsync(userId);
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return _Respository.FindByNameAsync(userName);
        }

        public Task UpdateAsync(User user)
        {
            return _Respository.UpdateAsync(user);
        }
        #endregion

        #region IUserRoleStore Implementation
        public Task<IList<string>> GetRolesAsync(User user)
        {
            return _Respository.GetRolesAsync(user);
        }

        public Task<bool> IsInRoleAsync(User user, string roleName)
        {
            return _Respository.IsInRoleAsync(user, roleName);
        }

        public Task RemoveFromRoleAsync(User user, string roleName)
        {
            return _Respository.RemoveFromRoleAsync(user, roleName);
        }

        public Task AddToRoleAsync(User user, string roleName)
        {
            return _Respository.AddToRoleAsync(user, roleName);
        }
        #endregion

        # region IUserPasswordStore Implementation
        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            return _Respository.SetPasswordHashAsync(user, passwordHash);
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return _Respository.GetPasswordHashAsync(user);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return _Respository.HasPasswordAsync(user);
        }
        #endregion

        #region IUserPhoneNumberStore Implementation

        public Task SetPhoneNumberAsync(User user, string phoneNumber)
        {
            return _Respository.SetPhoneNumberAsync(user, phoneNumber);
        }

        public Task<string> GetPhoneNumberAsync(User user)
        {
            return _Respository.GetPhoneNumberAsync(user);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(User user)
        {
            return _Respository.GetPhoneNumberConfirmedAsync(user);
        }

        public Task SetPhoneNumberConfirmedAsync(User user, bool confirmed)
        {
            return _Respository.SetPhoneNumberConfirmedAsync(user, confirmed);
        }

        #endregion

        #region IQueryableUserStore Implementation
        public IQueryable<User> Users
        {
            get
            {
                return _Respository.Users;
            }
        }
        #endregion

        #region IUserLoginStore Implementation
        public Task AddLoginAsync(User user, UserLoginInfo login)
        {
            return _Respository.AddLoginAsync(user, login);
        }

        public Task RemoveLoginAsync(User user, UserLoginInfo login)
        {
            return _Respository.RemoveLoginAsync(user, login);
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(User user)
        {
            return _Respository.GetLoginsAsync(user);
        }

        public Task<User> FindAsync(UserLoginInfo login)
        {
            return _Respository.FindAsync(login);
        }
        #endregion
        #region IUserTwoFactorStore Implementation
        public Task SetTwoFactorEnabledAsync(User user, bool enabled)
        {
            return _Respository.SetTwoFactorEnabledAsync(user, enabled);
        }

        public Task<bool> GetTwoFactorEnabledAsync(User user)
        {
            return _Respository.GetTwoFactorEnabledAsync(user);
        }
        #endregion

        #region IUserLockoutStore Implementation
        public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        {
            return _Respository.GetLockoutEndDateAsync(user);
        }

        public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        {
            return _Respository.SetLockoutEndDateAsync(user, lockoutEnd);
        }

        public Task<int> IncrementAccessFailedCountAsync(User user)
        {
            return _Respository.IncrementAccessFailedCountAsync(user);
        }

        public Task ResetAccessFailedCountAsync(User user)
        {
            return _Respository.ResetAccessFailedCountAsync(user);
        }

        public Task<int> GetAccessFailedCountAsync(User user)
        {
            return _Respository.GetAccessFailedCountAsync(user);
        }

        public Task<bool> GetLockoutEnabledAsync(User user)
        {
            return _Respository.GetLockoutEnabledAsync(user);
        }

        public Task SetLockoutEnabledAsync(User user, bool enabled)
        {
            return _Respository.SetLockoutEnabledAsync(user, enabled);
        }
        #endregion

        #region IUserEmailStore implementation
        public Task SetEmailAsync(User user, string email)
        {
            return _Respository.SetEmailAsync(user, email);
        }

        public Task<string> GetEmailAsync(User user)
        {
            return _Respository.GetEmailAsync(user);
        }

        public Task<bool> GetEmailConfirmedAsync(User user)
        {
            return _Respository.GetEmailConfirmedAsync(user);
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed)
        {
            return _Respository.SetEmailConfirmedAsync(user, confirmed);
        }

        public Task<User> FindByEmailAsync(string email)
        {
            return _Respository.FindByEmailAsync(email);
        }
        #endregion
    }
}