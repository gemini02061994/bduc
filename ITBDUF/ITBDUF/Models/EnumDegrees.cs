﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITBDUF.Models
{
    public enum EnumDegrees
    {
        Bachelor = 1,
        Masters = 2,
        Doctor = 3,
    }
}