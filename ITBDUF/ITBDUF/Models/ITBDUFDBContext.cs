﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ITBDUF.Models
{
    public class ITBDUFDBContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public static ITBDUFDBContext Create()
        {
            return new ITBDUFDBContext();
        }
    }
}