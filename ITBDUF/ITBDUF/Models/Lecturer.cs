﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITBDUF.Models
{
    public class Lecturer : IModel<int>
    {
        public int Id { get; set; }
        public EnumAcademicRanks AcademicRank { get; set; }
        public EnumDegrees Degree { get; set; }
        public virtual User User { get; set; }
        public virtual IEnumerable<Class> TeachingClasses { get; set; }
    }
}