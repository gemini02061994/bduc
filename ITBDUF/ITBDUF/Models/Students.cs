﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITBDUF.Models
{
    public class Student : IModel<int>
    {
        public int Id { get; set; }
        public string StudentCode { get; set; }
        public virtual IEnumerable<Class> Class { get; set; }
        public virtual User User { get; set; }
    }
}