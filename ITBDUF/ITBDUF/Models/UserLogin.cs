﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ITBDUF.Models
{
    public class CustomUserLogin
    {
        [System.ComponentModel.DataAnnotations.Key, Column(Order = 0)]
        public string LoginProvider { get; set; }

        [System.ComponentModel.DataAnnotations.Key, Column(Order =1)]
        public string ProviderKey { get; set; }

        [System.ComponentModel.DataAnnotations.Key, Column(Order =2)]
        public virtual User User { get; set;}

        public CustomUserLogin(UserLoginInfo info)
        {
            this.LoginProvider = info.LoginProvider;
            this.ProviderKey = info.ProviderKey;
        }
    }
}